FROM openjdk:8-jre-alpine
WORKDIR /app
ADD ./build/libs/readingo-seed-0.0.1.jar .
ENTRYPOINT ["java", "-jar", "readingo-seed-0.0.1.jar"]