@file:JvmName("Seeder")

package com.readingo

import com.mongodb.client.MongoCollection
import org.bson.BsonObjectId
import org.bson.Document
import org.litote.kmongo.KMongo
import org.litote.kmongo.deleteMany
import java.sql.Connection
import java.sql.DriverManager
import java.util.*
import java.util.logging.Level
import java.util.logging.Logger
import kotlin.concurrent.thread


fun main() {
    val connectionStringPostgres: String =
        System.getenv("CONN_STRING_POSTGRES") ?: "jdbc:postgresql://localhost:5432/postgres"
    val connectionStringMongo: String = System.getenv("CONN_HOST_MONGO") ?: "localhost"
    val bookIDS = generateBSONIDS(300)
    val communityIDS = generateIDS(100)
    val userIDS = generateIDS(300)

    val mongoThread = onMongo(connectionStringMongo) { mongoCollection ->
        println("Clean up old book tables...")
        cleanMongoTables(mongoCollection)
        println("Inserting books...")
        prepareBooks(mongoCollection, bookIDS)
    }

    val postgresThread = onPostgres(connectionStringPostgres) {
        println("Clean up old community tables...")
        cleanTables(it)
        println("Inserting communities...")
        prepareCommunities(it, communityIDS)
        println("Inserting users...")
        prepareUsers(it, userIDS, communityIDS)
        println("Inserting mappings...")
        prepareUserCommunityConnections(it, userIDS, communityIDS)
        println("Inserting books to communities...")
        prepareCommunityBooks(it, communityIDS, bookIDS)
        println("Done.")
    }

    listOf(mongoThread, postgresThread).forEach { it.join() }
}

fun prepareBooks(coll: MongoCollection<Document>, bookIDS: List<BsonObjectId>) {
    val docs = mutableListOf<Document>()
    val names = getNames()
    getTitles().forEachIndexed { index, line ->
        docs.add(
            Document(
                mapOf(
                    "_id" to bookIDS[index],
                    "title" to line,
                    "author" to names[index],
                    "thumbnail" to "https://dummyimage.com/200x300/${generateColor()}/ffffff.png&text=${line.replace(
                        ' ',
                        '+'
                    )}",
                    "pageCount" to Random().nextInt(300),
                    "__v" to 0
                )
            )
        )
    }
    coll.insertMany(docs)
}

fun onMongo(host: String, block: (MongoCollection<Document>) -> Unit): Thread {
    return thread {
        val mongoLogger: Logger = Logger.getLogger("org.mongodb.driver")
        mongoLogger.level = Level.SEVERE
        val conn = KMongo.createClient(host)
        val db = conn.getDatabase("readingo")
        val bookCollection = db.getCollection("books")
        block(bookCollection)
        conn.close()
    }
}

fun onPostgres(connectionString: String, block: (Connection) -> Unit): Thread {
    return thread {
        DriverManager.registerDriver(org.postgresql.Driver())
        val connection: Connection =
            DriverManager.getConnection(connectionString, "postgres", "password")
                ?: throw Exception("Can not connect to DB")
        connection.use { block(it) }
    }
}

fun cleanTables(connection: Connection) {
    connection.run {
        createStatement().executeUpdate("DELETE FROM community_members;")
        createStatement().executeUpdate("DELETE FROM community_user;")
        createStatement().executeUpdate("DELETE FROM community_books;")
        createStatement().executeUpdate("DELETE FROM community;")
    }
}

fun generateIDS(n: Int): List<String> {
    val ret = mutableListOf<String>()
    repeat(n) {
        ret.add(UUID.randomUUID().toString())
    }
    return ret.toList()
}

fun generateBSONIDS(n: Int): List<BsonObjectId> {
    val ret = mutableListOf<BsonObjectId>()
    repeat(n) {
        ret.add(BsonObjectId())
    }
    return ret.toList()
}

fun generateColor(): String {
    val hex = charArrayOf(
        '0', '1', '2', '3', '4', '5', '6', '7',
        '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'
    )
    val s = CharArray(6)
    var n = Random().nextInt(0x1000000)
    for (i in 0..5) {
        s[i] = hex[n and 0xf]
        n = n shr 4
    }
    return String(s)
}

fun prepareCommunities(
    connection: Connection,
    communityIDS: List<String>
) =
    getCommunities().forEachIndexed { index, line ->
        connection.prepareStatement(
            "INSERT INTO community(id, description, name) VALUES (?, ?, ?)"
        ).use { stmt ->
            stmt.apply {
                setString(1, communityIDS[index])
                setString(2, "$line community")
                setString(3, line)
            }
            stmt.execute()
        }
    }

fun prepareUsers(
    connection: Connection,
    userIDS: List<String>,
    communityIDS: List<String>
) =
    getNames().forEachIndexed { index, line ->
        connection.prepareStatement(
            "INSERT INTO community_user(id, email, username, community_id, password_hash) VALUES (?, ?, ?, ?, ?)"
        ).use { stmt ->
            stmt.apply {
                val username = line.replace(' ', '.')
                setString(1, userIDS[index])
                setString(2, "$username@mail.com")
                setString(3, username)
                setString(4, communityIDS[index / 3])
                setString(5, "\$2y\$12\$SwOo4yd.0c1bkC.6NnLq9.gDewdr9PdEoDbVnFXWJ2JdDVGTqhbqu")
            }
            stmt.execute()
        }
    }

fun prepareUserCommunityConnections(
    connection: Connection,
    userIDS: List<String>,
    communityIDS: List<String>
) =
    userIDS.forEachIndexed { index, user ->
        connection.prepareStatement(
            "INSERT INTO community_members(community_id, members_id)  VALUES (?, ?)"
        ).use { stmt ->
            stmt.apply {
                setString(1, communityIDS[index / 3])
                setString(2, user)
            }
            stmt.execute()
        }
    }

fun prepareCommunityBooks(
    connection: Connection,
    communityIDS: List<String>,
    bookIDS: List<BsonObjectId>
) =
    bookIDS.forEachIndexed { index, book ->
        connection.prepareStatement(
            "INSERT INTO community_books(community_id, books)  VALUES (?, ?)"
        ).use { stmt ->
            stmt.apply {
                setString(1, communityIDS[index / 3])
                setString(2, book.asObjectId().value.toString())
            }
            stmt.execute()
        }
    }

fun cleanMongoTables(coll: MongoCollection<Document>) {
    coll.deleteMany("{}")
}